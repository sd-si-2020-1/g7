# Grupo 7: Prontuário Eletrônico para Bovinos

----

## Descrição do projeto:

O prontuário auxilia veterinários na prestação de cuidados ao animais, sem ele a continuidade no atendimento e tratamento fica comprometida, dada a falta de informações básicas a respeito do animal.
Como muitas propriedades rurais pequenas não possuem um controle do rebanho o Prontuário Eletrônico irá facilitar essa gestão, visto que poderá ser feito no campo a partir de um celular, e acompanhar o desenvolvimento do animal.

## Membros:

- ~~Alfredo Maia~~
- ~~Caio Riserio da Silva~~
- Daniel Macedo Figueroa (Desenvolvedor)
- Gustavo Alves de Souza (Documentação)
- Luana Silva Carvalho (Coordenação/Desenvolvimento)

# Requisitos organizacionais

* **Não faça commit direto no master**, abra uma branch, faça commits na branch, e depois abra um pull request para dar merge da sua branch no master.

* **Commits só serão aceitos se eles passarem nos testes de integração automatizados**. 

* **Todo pull request deve referenciar uma issue**.

# Requisitos técnicos

* Back-end: [Node](https://nodejs.org/en/)

* Documentação da API: Open API com [Swagger](https://swagger.io/)

* Banco de dados: [MongoDB](https://www.mongodb.com/)

- Front-end: [React Native](https://reactnative.dev/)
    - React Native >= 0.63
    - [React Native Cli - Setup](https://reactnative.dev/docs/environment-setup)

* Wireframes e mockups: [Figma](https://www.figma.com/)

# Instalação e Execução
Siga os passos abaixo para executar a aplicação em seu smartphone físico.

Primeiramente, tenha instalado em sua máquina o [Node](https://nodejs.org/en/) e [MongoDB](https://www.mongodb.com/1) e siga os passos abaixo. Instruções para execução usando o Docker estão disponíveis em [backend](/Backend/README.md).

## Inicialize o backend:

### Instalar
```
git clone https://gitlab.com/sd-si-2020-1/g7.git
npm install
```

### Executar:
```
cd g7/api
npm run dev
```

### Testar:
Após executar acesse http://localhost:3000/api-docs/, veja a documentação no Swagger do principais serviços da API e execute testes.



## Inicialize o frontend (com React Native Cli):

### Instalar
```
cd g7/front
npm install
```

### Executar:
```
cd g7/api
npm run android
```


## Manuais e Tutoriais

* [Manual do desenvolvedor - Segurança e JWT](Documentos/Casos%20de%20Uso/CasosDeUso.md)
* [Protótipos do Prontuário Bovino](Prot%C3%B3tipo/Prontu%C3%A1rio%20Bovino%20-%20Prot%C3%B3tipo.pdf)



# Contato

Qualquer dúvida ou sugestão, entre em contado com os integrantes do projeto, ou abra uma nova [issue](https://gitlab.com/sd-si-2020-1/g7/-/issues). 

