import {StyleSheet} from 'react-native';
import commonStyles from './commonStyles';

const fontFamily = 'Lato';
const colors = {
  secondary: '#FFF',
  primary: '#266A2E',
  oKbutton: '#4571DA',
  link: '#0033CC',
};

const styles = StyleSheet.create({
  title: {...commonStyles.titles.titleBig},
  subtitle: {...commonStyles.titles.subtitle},
  formContainer: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    padding: 10,
    width: '90%',
  },
  input: {
    marginTop: 10,
    backgroundColor: '#FFF',
    marginBottom: 5,
    padding: 5,
  },
  button: {
    backgroundColor: commonStyles.colors.oKbutton,
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: commonStyles.fontFamily,
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold',
  },
  urlText: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.link,
    fontSize: 20,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
});

export default {styles};
