const fontFamily = 'Lato';
const colors = {
  secondary: '#FFF',
  primary: '#266A2E',
  oKbutton: '#4571DA',
  link: '#0033CC',
};

const titles = {
  titleBig: {
    fontFamily: fontFamily,
    color: colors.primary,
    fontSize: 48,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  title: {
    fontFamily: fontFamily,
    color: colors.primary,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  subtitle: {
    fontFamily: fontFamily,
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
};

export default {fontFamily, colors, titles};
