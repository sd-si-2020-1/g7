import React from 'react'
import {ImageBackground, StyleSheet } from 'react-native'

import backgroundImage from '../../assets/imgs/cow-login.jpg'

export default props => {
    return (<ImageBackground source={backgroundImage} style={[styles.background, props.style]}>
                {props.children}
            </ImageBackground>
            );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
});