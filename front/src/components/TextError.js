import React from 'react'
import {Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'

export default props => {
    if(props.visible){
        return (
            <Text style={styles.textError} {...props}>
                <Icon name='exclamation-circle'/> {props.text}
            </Text>
        )
    }
    else {
        return null;
    }
}

const styles = StyleSheet.create({
    textError:{
        color: '#FF3333',
        fontWeight: "bold",
    }
})