import React from 'react'
import {
    StyleSheet,
    View,
    ScrollView,
    TouchableOpacity,
  } from 'react-native';
import { Table, Row } from 'react-native-table-component';


    const getTableHead = (fields) => {
        const tableHead = []
        fields.forEach(field => {
            tableHead.push(field.title);
        });
        return tableHead;
    }

    const getWidthCols = (fields) =>{
        const widthCols = [];
        fields.forEach(field => {
            var width = field.width;
            if(!width){
              width = 100; //Valor default para o tamanho da coluna
            }
            widthCols.push(width);
        });
        return widthCols;
    }

    const getTableData = (fields, data) => {
        const tableData = [];
        for (let i = 0; i < data.length; i += 1) {  
            var rowData = {}
            fields.forEach(field => {
            rowData[field.key] =  data[i][field.key]
            }) 
            tableData.push(rowData);
        }
        return tableData;
    }

    const getDataRows = (fields, data) => {
        return data.map(row => fields.map(field => row[field.key]))
    }

  

export default props => {
    
    var hasData = false;
    if(props.data){
      hasData = (props.data.length> 0)
    }
    
    var dataRows = [];
    var tableData = [];
    
    if(hasData){
      dataRows = getDataRows(props.fields, props.data);
      tableData = getTableData(props.fields, props.data);
    }
    const tableHead = getTableHead(props.fields);
    const widthCols = getWidthCols(props.fields);

    return (
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{borderColor: '#C1C0B9'}}>
              <Row data={tableHead} widthArr={widthCols} style={styles.header} textStyle={styles.headerText}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderColor: '#C1C0B9'}}>
                {
                  tableData.map((rowData, index) => (
                    <TouchableOpacity key={index} onPress={() => props.onPress(rowData)}>
                      <Row
                        key={index}
                        data={dataRows[index]}
                        widthArr={widthCols}
                        style={[styles.rowOdd, index%2 && styles.rowPair]}
                        textStyle={styles.text}
                      />
                    </TouchableOpacity>
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    //backgroundColor: '#212732',
    backgroundColor: 'rgba(0,0,0,0.8)',
    padding: 5,
    width: '100%',
  },
  header: {
    height: 50,
    backgroundColor: 'rgba(0,0,0,0.8)',
    //backgroundColor: '#242b38'
  },
  tableBorderStyle: {
    borderColor: '#C1C0B9'
  },
  headerText: {
    textAlign: 'center',
    fontWeight: '100',
    color: 'white',
  },
  text: {
    textAlign: 'center',
    fontWeight: '100',
    color: '#fefefe',
  },
  dataWrapper: {
    marginTop: -1
  },
  rowOdd: {
    height: 40,
    backgroundColor: '#2c3445'
  },
  rowPair: {
    height: 40,
    backgroundColor: '#212733'
  } 
});