import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'; //reference lib: https://reactnavigation.org/docs/4.x/stack-navigator/

import AuthOrApp from './screens/AuthOrApp';
import Auth from './screens/Auth';
import CadastroAnimalSelecao from './screens/CadastroAnimalSelecao';
import Home from './screens/Home';
import NotImplemented from './screens/NotImplemented';
import RelatorioSelecao from './screens/RelatorioSelecao';
import TestTable from './screens/TestTable';
import ListaVacas from './screens/ListaVacas';
import PesquisaScreen from './screens/Pesquisa';
import CadastroAnimal from './screens/CadastroAnimal';
import CadastroVacina from './screens/CadastroVacina';
import ListaVacinas from './screens/ListaVacinas';
import Vacina from './screens/Vacina';

const mainStackRoutes = {
  Home: {
    name: 'Home2',
    screen: Home,
    navigationOptions: {headerShown: false},
  },
  NovoAnimalOptions: {
    name: 'NovoAnimal',
    screen: CadastroAnimalSelecao,
    navigationOptions: {title: 'Cadastro Novo Animal'},
  },
  NovoAnimal: {
    name: 'CadastroAnimal',
    screen: CadastroAnimal,
    navigationOptions: {title: 'Cadastro Animal'},
  },
  Vacina: {
    name: 'Vacina',
    screen: Vacina,
    navigationOptions: {title: 'Cadastro Vacina'},
  },
  NovaVacina: {
    name: 'CadastroVacina',
    screen: CadastroVacina,
    navigationOptions: {title: 'Cadastro Vacina'},
  },
  ListaVacinas: {
    name: 'ListaVacinas',
    screen: ListaVacinas,
    navigationOptions: {title: 'Lista Vacinas'},
  },
  NotImplemented: {
    name: 'Not Implemented',
    screen: NotImplemented,
    navigationOptions: {title: 'Brejo'},
  },
  RelatorioSelecao: {
    name: 'Relatorios',
    screen: RelatorioSelecao,
    navigationOptions: {title: 'Relatórios'},
  },
  ListarVacasLactantes: {
    name: 'ListaVacasLact',
    screen: ListaVacas,
    navigationOptions: {title: 'Lista Vacas Lactantes'},
  },
  ListarVacasSecas: {
    name: 'ListaVacasSecas',
    screen: ListaVacas,
    navigationOptions: {title: 'Lista Vacas Secas'},
  },
  Pesquisa: {
    name: 'Pesquisa',
    screen: PesquisaScreen,
    navigationOptions: {title: 'Pesquisa'},
  },
};

const mainStack = createStackNavigator(mainStackRoutes);

const mainRoutes = {
  AuthOrApp: {
    name: 'AuthOrApp',
    screen: AuthOrApp,
  },
  Auth: {
    name: 'Auth',
    screen: Auth,
  },
  Home: {
    name: 'Home',
    screen: mainStack,
  },
  TestTable: {
    name: 'TestTable',
    screen: TestTable,
  },
};

const mainNavigator = createSwitchNavigator(mainRoutes, {
  initialRouteName: 'AuthOrApp',
});

export default createAppContainer(mainNavigator);
