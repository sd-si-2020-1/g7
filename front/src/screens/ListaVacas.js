import React, {Component} from 'react';
import {StyleSheet, Text, FlatList, View} from 'react-native';

import commonStyles from '../utils/commonStyles.js';

//Importnando componentes próprios
import ImageBackgroundDefault from '../components/ImageBackgroundDefault';
import Axios from 'axios';
import {server, showError} from '../utils/commons';
import TableClickableRows from '../components/TableClickableRows';

export default class ListaVacas extends Component {
  fields = [
    {key: 'numBrinco', title: 'Numero do Brinco', width: 100},
    {key: 'nome', title: 'Nome Animal', width: 400},
  ];

  state = {
    data: [],
  };

  listaVacas = async () => {
    try {
      const result = await Axios.get(`${server}/api/allAnimals`);
      this.setState({data: result.data.animals});
    } catch (e) {
      console.log(e);
      showError(e);
    }
  };

  clickEventListener(item) {
    this.props.navigation.navigate(item.navTo);
  }

  onSelectRow(rowData) {
    console.log(rowData);
  }

  render() {
    this.listaVacas();
    return (
      <ImageBackgroundDefault>
        <TableClickableRows
          fields={this.fields}
          data={this.state.data}
          onPress={this.onSelectRow}
        />
      </ImageBackgroundDefault>
    );
  }
}
