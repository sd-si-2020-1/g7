import React, {Component} from 'react';

import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import formStyles from '../utils/formStyles.js';

//Importnando componentes próprios
import ImageBackgroundDefault from '../components/ImageBackgroundDefault';
import {showError} from '../utils/commons.js';
import TextError from '../components/TextError';
import Axios from 'axios';
import {server, showSucess} from '../utils/commons';

const initialState = {
  nome: '',
  dataAplicacao: '',
};

const initialValidationState = {
  invalidNome: false,
  invalidDataAplicacao: false,
};

export default class CadastroVacina extends Component {
  state = {
    ...initialState,
    ...initialValidationState,
  };

  cadastraVacina = async () => {
    try {
      await Axios.post(`${server}/api/vaccineRegister`, {
        nome: this.state.nome,
        dataAplicacao: this.state.dataAplicacao,
      });

      showSucess('Vacina cadastrada com sucesso');
      this.props.navigation.navigate('Home');
    } catch (e) {
      console.log(e);
      showError(e);
    }
  };

  render() {
    return (
      <ImageBackgroundDefault>
        <View style={formStyles.styles.formContainer}>
          <Text style={formStyles.styles.subtitle}>
            Preencha os dados do animal
          </Text>
          <TextInput
            placeholder="Nome da vacina"
            value={this.state.nome}
            style={formStyles.styles.input}
            onChangeText={(nome) => this.setState({nome})}
          />
          <TextError
            visible={this.state.invalidNome}
            text="O nome da vacina deve ser informado!"
          />

          <TextInput
            placeholder="Data de aplicação"
            value={this.state.dataAplicacao}
            style={formStyles.styles.input}
            onChangeText={(dataAplicacao) => this.setState({dataAplicacao})}
          />
          <TextError
            visible={this.state.invalidDataAplicacao}
            text="A data de aplicação deve ser informada!"
          />

          <TouchableOpacity onPress={this.cadastraVacina}>
            <View style={formStyles.styles.button}>
              <Text style={formStyles.styles.buttonText}>{'Cadastrar'}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ImageBackgroundDefault>
    );
  }
}
