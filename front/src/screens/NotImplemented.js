import React, {Component} from 'react';
import {ImageBackground, StyleSheet, View, Text} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import vacaBrejo from '../../assets/imgs/vacaBrejo.png';
import commonStyles from '../utils/commonStyles.js';

//Importando componentes próprios
import ImageBackgroundDefault from '../components/ImageBackgroundDefault';

export default class NotImplemented extends Component {
  render() {
    return (
      <ImageBackgroundDefault>
        <View style={styles.formContainer}>
          <Icon name="exclamation-circle" size={50} color="#E74C3C" />
          <Text style={styles.title}>Desculpe os transtornos!</Text>
          <Text style={styles.subtitle}>
            Nossa equipe ainda está trabalhando na construção dessa
            funcionalidade!
          </Text>
        </View>
        <ImageBackground source={vacaBrejo} style={styles.image} />
      </ImageBackgroundDefault>
    );
  }
}

const styles = StyleSheet.create({
  title: {...commonStyles.titles.title},
  subtitle: {...commonStyles.titles.subtitle},
  image: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.8)',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 50,
    width: '100%',
  },
});
