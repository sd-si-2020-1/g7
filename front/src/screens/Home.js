/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';

import Axios from 'axios';
import AssyncStorage from '@react-native-community/async-storage';

import commonStyles from '../utils/commonStyles.js';

//Importnando componentes próprios
import MyFlatList from '../components/MyFlatList';
import ImageBackgroundDefault from '../components/ImageBackgroundDefault';
import {showError} from '../utils/commons.js';

//Imagens obtidas em https://icons8.com/icon/set/heal/color
import newCow from '../../assets/imgs/new-cow.png';
import vacina from '../../assets/imgs/vacina.png';
import vacasLactante from '../../assets/imgs/vaca_Lact.png';
import vacasSecas from '../../assets/imgs/vaca_seca.png';
import bezerro from '../../assets/imgs/pacifier.png';
import pesquisar from '../../assets/imgs/pesquisar.png';
import report from '../../assets/imgs/report.png';
import logout from '../../assets/imgs/logout.png';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [

        {
            id: 1,
            title: 'Lactantes',
            image: vacasLactante,
            navTo: 'ListarVacasLactantes',
        },
        {
            id: 2,
            title: 'Vacas Secas',
            image: vacasSecas,
            navTo: 'ListarVacasSecas',
        },
        {
            id: 3,
            title: 'Bezerros',
            image: bezerro,
            navTo: 'NotImplemented',
        },
        {
            id: 4,
            title: 'Vacinas',
            image: vacina,
            navTo: 'Vacina',
        },
        {
            id: 5,
            title: 'Relatórios',
            image: report,
            navTo: 'RelatorioSelecao',
        },
        {
            id: 6,
            title: 'Novo Animal',
            image: newCow,
            navTo: 'NovoAnimalOptions',
        },
        {
            id: 7,
            title: 'Pesquisar',
            image: pesquisar,
            navTo: 'Pesquisar',
        },
        {
            id: 8,
            title: 'Sair',
            image: logout,
            navTo: 'logout',
        },
      ],
    };
  }

  clickEventListener(item) {

    if (!item.navTo){
        showError('Desculpe... Opção não implementada.');
        return;
    }

    if (item.navTo === 'logout'){
        this.logout();
        return;
    } else {
      try {
        this.props.navigation.navigate(item.navTo);
      } catch (error) {
        console.log(error)
      }
        
    }
  }

  logout = () => {
    delete Axios.defaults.headers.common.Authorization;
    AssyncStorage.removeItem('userData');
    this.props.navigation.navigate('AuthOrApp');
  }

  render() {
    return (
        <ImageBackgroundDefault>
            <Text style={styles.title}>Prontuário{'\n'}Bovino</Text>
            <MyFlatList data={this.state.data} onPress={this.clickEventListener.bind(this)}/>
        </ImageBackgroundDefault>
    );
  }
}

const styles = StyleSheet.create({
    title: { ...commonStyles.titles.title},
});
