import React, {Component} from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';

import Axios from 'axios';
import AssyncStorage from '@react-native-community/async-storage';

export default class AuthOrApp extends Component {
  componentDidMount = async () => {
    const userDataJason = await AssyncStorage.getItem('userData');
    let userData = null;

    try {
      userData = JSON.parse(userDataJason);
    } catch (error) {
      //o erro será ignorado pois o que importa é manter o userData null
      userData = null;
    }
    if (userData && userData.token) {
      Axios.defaults.headers.common.Authorization = `Bearer ${userData.token}`;
      this.props.navigation.navigate('Home', userData);
    } else {
      this.props.navigation.navigate('Auth');
    }
  };

  render() {
    return (
      <View style={style.container}>
        <ActivityIndicator size="large" color="green" />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
});
