import React, {Component} from 'react';

import {Text, StyleSheet, View} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import RadioButtonRN from 'radio-buttons-react-native'; //https://github.com/sramezani/radio-buttons-react-native

import commonStyles from '../utils/commonStyles.js';

//Importnando componentes próprios
import ImageBackgroundDefault from '../components/ImageBackgroundDefault';
import {showError} from '../utils/commons.js';

const data = [
  {
    label: 'Nascimento',
    navTo: 'NovoAnimal',
  },
  {
    label: 'Compra',
    navTo: 'NovoAnimal',
  },
];

export default class CadastroAnimalSelecao extends Component {
  selectedEvent(item) {
    if (!item.navTo) {
      showError('Desculpe... Opção não implementada.');
      return;
    }
    this.props.navigation.navigate(item.navTo);
  }

  render() {
    return (
      <ImageBackgroundDefault>
        <View style={styles.formContainer}>
          <Text style={styles.subtitle}>Qual a origem do animal?</Text>
          <RadioButtonRN
            data={data}
            activeColor="#145A32"
            animationTypes={['shake']}
            selectedBtn={(item) => this.selectedEvent(item)}
            circleSize={16}
            boxActiveBgColor="#ABEBC6"
            icon={<Icon name="check-circle" size={25} color="#145A32" />}
          />
        </View>
      </ImageBackgroundDefault>
    );
  }
}

const styles = StyleSheet.create({
  title: {...commonStyles.titles.title},
  subtitle: {...commonStyles.titles.subtitle},
  formContainer: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    padding: 10,
    width: '90%',
  },
  button: {
    backgroundColor: commonStyles.colors.oKbutton,
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: commonStyles.fontFamily,
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
