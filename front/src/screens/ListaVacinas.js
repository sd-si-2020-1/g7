import React, {Component} from 'react';
import {StyleSheet, Text, FlatList, View} from 'react-native';

import commonStyles from '../utils/commonStyles.js';

//Importnando componentes próprios
import ImageBackgroundDefault from '../components/ImageBackgroundDefault';
import Axios from 'axios';
import {server, showError} from '../utils/commons';
import TableClickableRows from '../components/TableClickableRows';

export default class ListaVacinas extends Component {
  fields = [
    {key: 'nome', title: 'Nome da vacina', width: 100},
    {key: 'dataAplicacao', title: 'Data aplicação', width: 400},
  ];

  state = {
    data: [],
  };

  listaVacinas = async () => {
    try {
      const result = await Axios.get(`${server}/api/allVaccines`);
      this.setState({data: result.data.vaccines});
    } catch (e) {
      console.log(e);
      showError(e);
    }
  };

  clickEventListener(item) {
    this.props.navigation.navigate(item.navTo);
  }

  onSelectRow(rowData) {
    console.log(rowData);
  }

  render() {
    this.listaVacinas();
    return (
      <ImageBackgroundDefault>
        <TableClickableRows
          fields={this.fields}
          data={this.state.data}
          onPress={this.onSelectRow}
        />
      </ImageBackgroundDefault>
    );
  }
}
