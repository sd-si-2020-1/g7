/*This is an Example of Grid View in React Native*/
import React, { Component } from 'react';
import {
  View
} from 'react-native';

import TableClickableRows from './../components/TableClickableRows'

export default class App extends Component {
  constructor(props) {
    super(props);

    this.fields = [
      { key: 'code', title: 'MARCA', width: 200 },
      { key: 'responsable', title: 'RESPONSABLE', width: 150 },
      { key: 'piezas', title: 'PZA', width: 100 },
      { key: 'peso', title: 'KG', width: 100 },
      { key: 'inicio', title: 'INICIO', width: 100 },
      { key: 'termino', title: 'ENTREGA', width: 100 },
      { key: 'hab', title: 'HABILITADO', width: 100 },
      { key: 'arm', title: 'ARMADO', width: 100 },
      { key: 'bar', title: 'BARRENADO', width: 100 },
      { key: 'sol', title: 'SOLDADO', width: 100 },
      { key: 'insp', title: 'LIBERACIÓN', width: 100 },
    ];

    this.rows = Array.apply(null, Array(50)).map(
      (item, idx) => ({
        code: `TEST-MARK-AREA-X-ITEM-${idx}`,
        responsable: `RESPONSABLE-${idx}`,
        piezas: 1,
        peso: idx * 312,
        inicio: '2019-02-01',
        termino: '2019-04-30',
        hab: 0,
        arm: 0,
        bar: 0,
        sol: 0,
        insp: 0,
      })
    );

    this.state = {
      data: this.rows.map(row =>
        this.fields.map(field => row[field.key])
      ),
      tableHead: this.fields.map(field => field.title),
      widthArr: this.fields.map(field => field.width)
    };
  }

  onSelectRow(rowData) {
    console.log(rowData.responsable)
  }

  render(){
    return (
      <TableClickableRows
        fields = {this.fields} 
        data = {this.rows}
        onPress = {this.onSelectRow}
      />
    )
  }

  render1(){
    
    const hasData = (props.data.count() > 0)
    if(hasData){
      const dataRows = getDataRows(props.fields, props.data);
      const tableData = getTableData(props.fields, props.data);
    }
    const tableHead = getTableHead(props.fields);
    const widthCols = getWidthCols(props.fields);

    return (
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{borderColor: '#C1C0B9'}}>
              <Row data={tableHead} widthArr={widthCols} style={styles.header} textStyle={styles.headerText}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderColor: '#C1C0B9'}}>
                {[hasData &&
                  tableData.map((rowData, index) => (
                    <TouchableOpacity key={index} onPress={() => props.onPress(rowData)}>
                      <Row
                        key={index}
                        data={dataRows[index]}
                        widthArr={widthCols}
                        style={[styles.rowOdd, index%2 && styles.rowPair]}
                        textStyle={styles.text}
                      />      
                    </TouchableOpacity>
                  ))
                ]}
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }


}
