import React, {Component} from 'react';

import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import formStyles from '../utils/formStyles.js';

//Importnando componentes próprios
import ImageBackgroundDefault from '../components/ImageBackgroundDefault';
import {showError} from '../utils/commons.js';
import TextError from '../components/TextError';
import Axios from 'axios';
import {server, showSucess} from '../utils/commons';

const initialState = {
  nome: '',
  numBrinco: '',
  raca: '',
  sexo: '',
  medCadastro: '',
};

const initialValidationState = {
  invalidNome: false,
  invalidNumBrinco: false,
  invalidRaca: false,
  invalidSexo: false,
  invalidMedCadastro: false,
};

export default class CadastroAnimal extends Component {
  state = {
    ...initialState,
    ...initialValidationState,
  };

  cadastraAnimal = async () => {
    try {
      await Axios.post(`${server}/api/animalRegister`, {
        nome: this.state.nome,
        numBrinco: this.state.numBrinco,
        raca: this.state.raca,
        sexo: this.state.sexo,
        medCadastro: this.state.medCadastro,
      });

      showSucess('Animal cadastrado com sucesso');
      this.props.navigation.navigate('Home');
    } catch (e) {
      console.log(e);
      showError(e);
    }
  };

  render() {
    return (
      <ImageBackgroundDefault>
        <View style={formStyles.styles.formContainer}>
          <Text style={formStyles.styles.subtitle}>
            Preencha os dados do animal
          </Text>
          <TextInput
            placeholder="Nome"
            value={this.state.nome}
            style={formStyles.styles.input}
            onChangeText={(nome) => this.setState({nome})}
          />
          <TextError
            visible={this.state.invalidNome}
            text="O nome deve ser informado!"
          />

          <TextInput
            placeholder="Número do brinco"
            value={this.state.numBrinco}
            style={formStyles.styles.input}
            onChangeText={(numBrinco) => this.setState({numBrinco})}
          />
          <TextError
            visible={this.state.invalidNumBrinco}
            text="O número do brinco deve ser informado!"
          />

          <TextInput
            placeholder="Raça"
            value={this.state.raca}
            style={formStyles.styles.input}
            onChangeText={(raca) => this.setState({raca})}
          />
          <TextError
            visible={this.state.invalidRaca}
            text="A raça do animal deve ser informada!"
          />

          <TextInput
            placeholder="Sexo"
            value={this.state.sexo}
            style={formStyles.styles.input}
            onChangeText={(sexo) => this.setState({sexo})}
          />
          <TextError
            visible={this.state.invalidSexo}
            text="O sexo do animal deve ser informado!"
          />

          <TouchableOpacity onPress={this.cadastraAnimal}>
            <View style={formStyles.styles.button}>
              <Text style={formStyles.styles.buttonText}>{'Cadastrar'}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ImageBackgroundDefault>
    );
  }
}
