/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';

import commonStyles from '../utils/commonStyles.js'

//Importnando componentes próprios
import MyFlatList from '../components/MyFlatList'
import ImageBackgroundDefault from '../components/ImageBackgroundDefault'

//Imagens obtidas em https://icons8.com/icon/set/heal/color
import vendaAnimais from '../../assets/imgs/sales.png';
import vendaLeite from '../../assets/imgs/com_leite.png';
import compraAnimal from '../../assets/imgs/GetMoney.png';


export default class RelatorioSelecao extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [

        {
            id: 1,
            title: 'Vendas de Animais',
            image: vendaAnimais,
            navTo: 'NotImplemented',
        },
        {
            id: 2,
            title: 'Venda de Leite',
            image: vendaLeite,
            navTo: 'NotImplemented',
        },
        {
            id: 3,
            title: 'Compras de Animais',
            image: compraAnimal,
            navTo: 'NotImplemented',
        },
      ],
    };
  }

  clickEventListener(item,) {
    this.props.navigation.navigate(item.navTo);
  }

  render() {
    return (
        <ImageBackgroundDefault>
            <Text style={styles.title}>Prontuário{"\n"}Bovino</Text>
            <MyFlatList data={this.state.data} onPress={this.clickEventListener.bind(this)}/>
        </ImageBackgroundDefault>
    );
  }
}

const styles = StyleSheet.create({
    title: { ...commonStyles.titles.title},
});
