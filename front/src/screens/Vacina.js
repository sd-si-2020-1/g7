/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';

import commonStyles from '../utils/commonStyles.js'

//Importnando componentes próprios
import MyFlatList from '../components/MyFlatList'
import ImageBackgroundDefault from '../components/ImageBackgroundDefault'

//Imagens obtidas em https://icons8.com/icon/set/heal/color
import vacina from '../../assets/imgs/vacina.png';

export default class Vacina extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [

        {
            id: 1,
            title: 'Cadastro Vacina',
            image: vacina,
            navTo: 'NovaVacina',
        },
        {
            id: 2,
            title: 'Listagem Vacinas',
            image: vacina,
            navTo: 'ListaVacinas',
        },
      ],
    };
  }

  clickEventListener(item,) {
    this.props.navigation.navigate(item.navTo);
  }

  render() {
    return (
        <ImageBackgroundDefault>
            <Text style={styles.title}>Vacina</Text>
            <MyFlatList data={this.state.data} onPress={this.clickEventListener.bind(this)}/>
        </ImageBackgroundDefault>
    );
  }
}

const styles = StyleSheet.create({
    title: { ...commonStyles.titles.title},
});
