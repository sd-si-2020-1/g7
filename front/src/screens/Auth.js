import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import formStyles from '../utils/formStyles.js';

import Axios from 'axios';
import AssyncStorage from '@react-native-community/async-storage';

import ImageBackgroundDefault from '../components/ImageBackgroundDefault';

import TextError from '../components/TextError.js';
import {server, showError, showSucess} from '../utils/commons.js';

const initialState = {
  name: '',
  email: '',
  password: '',
  confirmPassword: '',
  newUser: false,
};

const initialValidationState = {
  ivalidName: false,
  invalidEmail: false,
  invalidPassword: false,
  notMathcPassword: false,
};

export default class Auth extends Component {
  state = {
    ...initialState,
    ...initialValidationState,
  };

  signinOrSignup = () => {
    if (this.camposValidos()) {
      if (this.state.newUser) {
        this.signUp();
      } else {
        this.singIn();
      }
    }
  };

  signUp = async () => {
    try {
      await Axios.post(`${server}/oapi/register`, {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        confirmPassword: this.state.confirmPassword,
      });

      showSucess('Usuário cadastrado');
      this.setState({...initialState});
    } catch (e) {
      console.log(e);
      showError(e);
    }
  };

  singIn = async () => {
    try {
      const res = await Axios.post(`${server}/oapi/authenticate`, {
        email: this.state.email,
        password: this.state.password,
      });

      AssyncStorage.setItem('userData', JSON.stringify(res.data));
      Axios.defaults.headers.common.Authorization = `${res.data.token}`;
      this.props.navigation.navigate('Home', res.data);
    } catch (e) {
      console.log(e);
      showError(e);
    }
  };

  camposValidos = () => {
    const Validations = [];

    const invalidEmail = !(this.state.email && this.state.email.includes('@'));

    let invalidPassword = !(
      this.state.password &&
      (this.state.newUser ? this.state.password.length >= 6 : true)
    );

    const ivalidName = this.state.newUser
      ? !(this.state.name && this.state.name.length >= 3)
      : false;

    const notMathcPassword = this.state.newUser
      ? !(this.state.password === this.state.confirmPassword)
      : false;

    this.setState({invalidEmail});
    this.setState({invalidPassword});
    this.setState({ivalidName});
    this.setState({notMathcPassword});

    Validations.push(!invalidEmail);
    Validations.push(!invalidPassword);
    Validations.push(!ivalidName);
    Validations.push(!notMathcPassword);

    return Validations.reduce((t, a) => t && a);
  };

  onPressAlterarNewUser = () => {
    this.setState({newUser: !this.state.newUser});
    this.setState({...initialValidationState});
  };

  render() {
    return (
      <ImageBackgroundDefault>
        <Text style={formStyles.styles.title}>Prontuário{'\n'}Bovino</Text>
        <View style={formStyles.styles.formContainer}>
          <Text style={formStyles.styles.subtitle}>
            {this.state.newUser
              ? 'Crie a sua conta'
              : 'Informe seus dados para entrar'}
          </Text>
          {this.state.newUser && (
            <TextInput
              placeholder="Nome"
              value={this.state.name}
              style={formStyles.styles.input}
              onChangeText={(name) => this.setState({name})}
            />
          )}
          <TextError
            visible={this.state.ivalidName}
            text="O nome deve ser informado!"
          />

          <TextInput
            placeholder="Usuário (E-mail)"
            value={this.state.email}
            style={formStyles.styles.input}
            onChangeText={(email) => this.setState({email})}
          />
          <TextError
            visible={this.state.invalidEmail}
            text="E-mail inválido!"
          />

          <TextInput
            placeholder="Senha"
            value={this.state.password}
            style={formStyles.styles.input}
            onChangeText={(password) => this.setState({password})}
            secureTextEntry={true}
          />
          <TextError
            visible={this.state.invalidPassword}
            text={
              this.state.newUser
                ? 'A senha deve conter 6 ou mais digitos!'
                : 'A senha deve ser informada!'
            }
          />

          {this.state.newUser && (
            <TextInput
              placeholder="Confirmação de Senha"
              value={this.state.confirmPassword}
              style={formStyles.styles.input}
              onChangeText={(confirmPassword) =>
                this.setState({confirmPassword})
              }
              secureTextEntry={true}
            />
          )}
          <TextError
            visible={this.state.notMathcPassword}
            text="A senha e a confirmação de senha não conferem!"
          />

          <TouchableOpacity onPress={this.signinOrSignup}>
            <View style={formStyles.styles.button}>
              <Text style={formStyles.styles.buttonText}>
                {this.state.newUser ? 'Registrar' : 'Entrar'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{padding: 10}}
          onPress={this.onPressAlterarNewUser}>
          <Text style={formStyles.styles.urlText}>
            {this.state.newUser
              ? 'Já possui conta?'
              : 'Ainda não possui conta?'}
          </Text>
        </TouchableOpacity>
      </ImageBackgroundDefault>
    );
  }
}
