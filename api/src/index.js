//Para auxiliar nas rotas e requests http
const express = require('express');

//Para lidar com as urls e a receber informação json
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require('./controllers/authController')();

app

console.log(app);

app.listen(3000);
