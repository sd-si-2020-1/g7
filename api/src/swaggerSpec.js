const swaggerJSDoc = require('swagger-jsdoc');

module.exports = function (porta) {
      const swaggerDefinition = {
        openapi: '3.0.0',
        info: {
          "version": "1.0.0",
          "title": "Prontuario Bovino"
        },
        servers: [
          {
            url: `http://localhost:${porta}`,
            description: 'Development server',
          },
        ],
        consumes: [
          "application/json"
        ],
        produces: [
          "application/json"
        ],
        "components": {
          "securitySchemes": {
            "bearerAuth": {
              "type": "http",
              "scheme": "bearer",
              "bearerFormat": "JWT"
            }
          }
        },
        "security": [
          {
            "bearerAuth": []
          }
        ]
      };

      const options = {
        swaggerDefinition,
        // Path to the API docs
        apis: ['./**/*.yaml'],
      }
    return swaggerJSDoc(options);
}