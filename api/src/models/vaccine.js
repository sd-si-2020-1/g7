const restful = require('node-restful')
const mongoose = restful.mongoose

const VaccineSchema = new mongoose.Schema({
    nome: { type: String, require: true },
    dataAplicacao: { type: Date, default: Date.now, required: true },

});

module.exports = mongoose.model('Vaccine', VaccineSchema);