const restful = require('node-restful')
const mongoose = restful.mongoose

const AnimalSchema = new mongoose.Schema({
    nome: { type: String, require: true },
    numBrinco: { type: String, unique: true, required: true },
    raca: { type: String, required: false },
    sexo: { type: String, required: true },
    dataCadastro: { type: Date, default: Date.now },
    medCadastro:{type: String, required: false},
});

module.exports = mongoose.model('Animal', AnimalSchema);
