const restful = require('node-restful')
const mongoose = restful.mongoose

const DiseaseSchema = new mongoose.Schema({
    nome: { type: String, require: true, unique: true },
    tratamento: { type: String, required: true },
    dataRegistro: { type: Date, required: true, default: Date.now },

});

module.exports = mongoose.model('Doenca', DiseaseSchema);;