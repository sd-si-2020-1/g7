const Disease = require('../models/disease');
const restifySwaggerJsdoc = require('restify-swagger-jsdoc');

Disease.updateOptions({new: true, runValidators: true})

const diseaseRegister = (req, res) => {
    const objt = req.body;
    const {nome, tratamento} = objt;

    try {
        const novaDoenca = new Disease(req.body);
        novaDoenca.save(function(err, doenca) {
            if (err) {
                if(err.keyPattern.nome){
                    return res.status(500).json({errors: ['Já existe doença cadastrado com este nome.']})
                }
                return res.status(500).send({errors: [err]});
            }
            res.send({doenca});
        });

    } catch (err) {
        return res.status(400).send({ errors: ['Falha no cadastro']});
    }
};

const diseaseList = (req, res) => {
    const {nome} = req.query;
    try {
        Disease.findOne({nome})
              .then(doenca=>{
                        if(!doenca){
                            return res.status(400).send({errors: ['Doença não encontrada']});
                        }
                        res.send({doenca});
                    })
    } catch (err) {
        return res.status(500).send({ errors: [err]});
    }
            
};

module.exports = {diseaseRegister, diseaseList}


