const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const tkConf = require('../config/auth.json');
const User = require('../models/user');

User.updateOptions({new: true, runValidators: true})


function generateToken(params = {}) {
    return jwt.sign(params, tkConf.secret, {
        //expiresIn: tkConf.expiresIn, Não expira
    });
}

const validateToken = (req, res) => {
    const token = req.body.token || ''
    jwt.verify(token, tkConf.secret, function (err, decoded) {
        return res.status(200).send({ valid: !err })
    })
}

const register = (req, res) => {
    const { name, email, password } = req.body;

    try {

        let newUser = new User({name : name, email: email, password : password});
        newUser.save(function(err, user) {

            if (err) {
                if(err.keyPattern.email){
                    return res.status(500).json({errors: ['Esse email já foi utilizado anteriormente']});
                }
                return res.status(500).send({errors: [err]});
            }

            user.password = undefined;

            res.send({
                user,
                token: generateToken({ id: user.id }),
            });
        });

    } catch (err) {
        return res.status(400).send({ errors: ["Erro ao cadastrar usuário", err.message] });
    }
}

const authenticate = (req, res) => {
    const { email, password } = req.body;

    try {
        User.findOne({email})
            .then(user=>{
                const invalidUser = !user
                let invalidPwd = new Boolean(false);
                const token = generateToken({ id: user.id });

                if(!invalidUser){
                    invalidPwd = !bcrypt.compareSync(password, user.password)
                }

                if(invalidUser || invalidPwd){
                    return res.status(400).send({errors: ['Usuário e/ou senha invalidos.']});
                }

                user.password = undefined;
                res.send({
                    user,
                    token: token,
                });
            })
    } catch (err) {
        return res.status(500).send({ errors: [err]});
    }
}

module.exports = {validateToken, register, authenticate};

