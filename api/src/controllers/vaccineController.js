const Vaccine = require('../models/vaccine');
const restifySwaggerJsdoc = require('restify-swagger-jsdoc');

Vaccine.updateOptions({new: true, runValidators: true})

const vaccineRegister = (req, res) => {
    try {
        const novaVacina = new Vaccine(req.body);
        novaVacina.save(function(err, novaVacina) {
            if (err) {
                return res.status(500).send({errors: err.toString()});
            }
            res.send({novaVacina});
        });

    } catch (err) {
        return res.status(400).send({ errors: ['Falha no cadastro']});
    }
};

const allVaccines = (req, res) => {
    try {
        Vaccine.find({})
            .then(vaccines => {
                if(!vaccines){
                    return res.status(400).send({errors: ['Vacinas não encontradas']});
                }
                res.send({vaccines});
            })
    } catch (err) {
        return res.status(500).send({ errors: err.toString()});
    }

};

module.exports = {vaccineRegister, allVaccines}


