const Animal = require('../models/animal');
const restifySwaggerJsdoc = require('restify-swagger-jsdoc');

Animal.updateOptions({new: true, runValidators: true})

const animalRegister = (req, res) => {
    try {
        const novoAnimal = new Animal(req.body);
        novoAnimal.save(function(err, animal) {
            if (err) {
                return res.status(500).send({errors: err.toString()});
            }
            res.send({animal});
        });

    } catch (err) {
        return res.status(400).send({ errors: ['Falha no cadastro']});
    }
};

const animalList = (req, res) => {
    const { numBrinco } = req.query;
    try {
        Animal.findOne({ numBrinco })
            .then(animal => {
                if(!animal){
                    return res.status(400).send({errors: ['Animal não encontrado']});
                }
                res.send({animal});
            })
    } catch (err) {
        return res.status(500).send({ errors: err.toString()});
    }

};

const allAnimals = (req, res) => {
    try {
        Animal.find({})
            .then(animals => {
                if(!animals){
                    return res.status(400).send({errors: ['Animal não encontrado']});
                }
                res.send({animals});
            })
    } catch (err) {
        return res.status(500).send({ errors: err.toString()});
    }

};

module.exports = {animalRegister, animalList, allAnimals}


