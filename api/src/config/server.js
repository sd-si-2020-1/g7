const porta = 3000

//Para lidar com as urls e a receber informação json
const bodyParser = require('body-parser');
//Para auxiliar nas rotas e requests http
const express = require('express');
const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));

server.listen(porta, function() {
    console.log(`Aplicação rodando na Porta ${porta}.`)
})
server.porta = porta;

module.exports = server
