const express = require('express')
const auth = require('./auth')
const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('./../swaggerSpec')

module.exports = function (server) {

    //Rota Swagger
    const swaggerDocument = require('../swaggerDoc.json');
    server.use('/api-docs-v0', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec(server.porta)));

    /*
        -- Rotas abertas da API
            Adicionar ao openApi somente rotas que não necessitam de autenticação
    */
    const openApi = express.Router()
    server.use('/oapi', openApi)

    const AuthController = require('../controllers/authController')
    openApi.post('/register', AuthController.register)
    openApi.post('/authenticate', AuthController.authenticate)
    openApi.post('/validateToken', AuthController.validateToken)

    /*
        -- Rotas protegidas por Token JWT
            Adicionar ao protectedApi somente rotas NECESSITEM de autenticação
    */
    //API routers
    const protectedApi = express.Router()
    server.use('/api', protectedApi)

    // Esta linha pode ser comentada para que possa ser realizar os testes sem a necessidade de ter um token
    protectedApi.use(auth)
    
    //Serviços Animal
    const AnimalController = require('../controllers/animalController')
    protectedApi.get('/animalList', AnimalController.animalList)
    protectedApi.get('/allAnimals', AnimalController.allAnimals)
    protectedApi.post('/animalRegister', AnimalController.animalRegister)

    //Serviços Doenças
    const DiseaseController = require('../controllers/diseaseController')
    protectedApi.get('/diseaseList', DiseaseController.diseaseList)
    protectedApi.post('/diseaseRegister', DiseaseController.diseaseRegister)

    //Serviços Vacinas
    const VaccineController = require('../controllers/vaccineController')
    protectedApi.get('/allVaccines', VaccineController.allVaccines)
    protectedApi.post('/vaccineRegister', VaccineController.vaccineRegister)

    
}