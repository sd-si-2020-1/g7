const jwt = require('jsonwebtoken')
const authConfig = require('./auth.json')

module.exports = (req, res, next) => {
    // CORS preflight request
    if (req.method === 'OPTIONS') {
        next()
    } else {
        var token = req.body.token || req.query.token
        
        if(!token){
            var bearerHeader = req.headers['authorization'];
            if (bearerHeader){
                token = bearerHeader.split(' ')[1]
                if(!token){
                    token = bearerHeader
                }
            }
        }

        if (!token) {
            return res.status(403).send({ errors: ['No token provided.'] })
        }
        
        jwt.verify(token, authConfig.secret, function (err, decoded) {
            if (err) {
                return res.status(403).send({
                    errors: err.toString()
                })
            } else {
                next()
            }
        })
    }
}
