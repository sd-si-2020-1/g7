const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

dblocal = true
port = ':27017'
endrecoDb = `://localhost${port}`
nomeDb = "prontuario"

if(!dblocal){
  adresServer = "cluster0.3djv7.mongodb.net"
  userName = "prontVetDb"
  pass = "@protVet1"
  endrecoDb = `+srv://${userName}:${pass}@${adresServer}`
}

stringConnection = `mongodb${endrecoDb}/${nomeDb}?retryWrites=true&w=majority`

console.log(`Mongo Connection: ${dblocal?'Local':'Cloud'}`)

module.exports = mongoose.connect(stringConnection, 
                                    {
                                        useNewUrlParser: true,
                                        useUnifiedTopology: true,
                                        useCreateIndex: true,
                                        useFindAndModify: false
                                    }
                                  );

mongoose.Error.messages.general.required = "O atributo '{PATH}' é obrigatório"
mongoose.Error.messages.Number.min = "O valor '{VALUE}' informado é menor que o limite mínimo de '{MIN}'"
mongoose.Error.messages.Number.max = "O valor '{VALUE}' informado é maior que o limite máximo de '{MAX}'"
mongoose.Error.messages.String.enum = "O valor '{VALUE}' informado não é válido para o atributo '{PATH}'"
mongoose.Error.messages.ValidatorError = "Falha de validação no objeto '{TYPE}':"