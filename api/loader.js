const server = require('./src/config/server')
require('./src/config/dbConnection')
require('./src/config/routes')(server)
