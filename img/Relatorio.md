* **Modelo: um objeto ou classe que tenha atributos e métodos de uma entidade, com pelo menos 2 atributos e 2 métodos** 

```javascript
const mongoose = require('../database/dbConnection');

const AnimalSchema = new mongoose.Schema({
    nome: {
        type: String,
        require: true,
    },
    numBrinco: {
        type: String,
        unique: true,
        required: true,
    },
    raca: {
        type: String,
        required: false,
    },
    sexo: {
        type: String,
        required: true,
        select: false,
    },
    dataCadastro: {
        type: Date,
        default: Date.now,
    },
   
});

AnimalSchema.pre('save', async function(next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;

    next();
});

const Animal = mongoose.model('Animal', AnimalSchema);

module.exports = Animal;
```

* **Interface: a interface REST para o seu modelo, com pelo menos 2 rotas** 

```javascript
const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Animal = require('../models/animal');
const restifySwaggerJsdoc = require('restify-swagger-jsdoc');

const router = express.Router();

restifySwaggerJsdoc.createSwaggerPage({
    title: 'Documentacao Animal', // Page title
    version: '1.0.0', // Server version
    server: server, // Restify server instance created with restify.createServer()
    path: '/auth/animalRegister'  // Public url where the swagger page will be available
});

router.post('/animalRegister', async (req, res) => {
    const {numBrinco, nome, raca, sexo} = req.body;

    try {
        if(await Animal.findOne({numBrinco}))
            return res.status(400).send({error: 'Animal já cadastrado'});
             
        const animal = await Animal.create(req.body);

        return res.send({animal});
    } catch (err) {
        return res.status(400).send({ error: 'Falha no cadastro'});
    }
});

router.get('/animalList', async (req, res) => {
    const {numBrinco} = req.body;

    const animal = await (await Animal.findId({numBrinco}));

    if(!animal)
        return res.status(400).send({error: 'Animal não encontrado'});

    res.send({animal});
         
});

module.exports = app => app.use('/auth', router);

```


* **Schema: criar o schema para gravar o seu objeto no banco de dados (Mongodb)** 

```javascript
const mongoose = require('../database/dbConnection');

const AnimalSchema = new mongoose.Schema({
    nome: {
        type: String,
        require: true,
    },
    numBrinco: {
        type: String,
        unique: true,
        required: true,
    },
    raca: {
        type: String,
        required: false,
    },
    sexo: {
        type: String,
        required: true,
        select: false,
    },
    dataCadastro: {
        type: Date,
        default: Date.now,
    },
   
});

AnimalSchema.pre('save', async function(next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;

    next();
});

const Animal = mongoose.model('Animal', AnimalSchema);

module.exports = Animal;
```

* **Documentação: a documentação OpenAPI do seu modelo, com interface interativa via Swagger**

```yaml
swagger: "2.0"
info:
  version: "1.0.0"
  title: Prontuario Bovino
host: localhost:3000
basePath: /app-docs
schemes:
  - http
  - https
consumes:
  - application/json
produces:
  - application/json
paths:
  /:
    x-swagger-router-controller: animal
    get:
      description: Retorna o animal solicitado.
      operationId: animalList
      responses:
        "200":
          description: Success
          schema:
            # a pointer to a definition
            $ref: "#/definitions/animal"
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
    post:
      description: Adicionar um novo animal.
      operationId: animalRegister
      parameters:
        - name: numBrinco
          in: query
          description: Numero do Brinco.
          required: true
          type: string
        - name: nome
          in: query
          description: Nome do animal
          required: true
          type: string
        - name: raca
          in: query
          description: Raca do animal
          required: true
          type: string
        - name: sexo
          in: query
          description: Sexo do animal
          required: true
          type: string      
      responses:
        "200":
          description: Success
          schema:
            # a pointer to a definition
            $ref: "#/definitions/Animal"
        # responses may fall through to errors
        default:
          description: Error
          schema:
            $ref: "#/definitions/ErrorResponse"
```
