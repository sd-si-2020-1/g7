# Login e Segurança
Na tela principal de longi, o usuário poderá realizar duas ações: Realizar Login ou Realizar Cadastro.

![](Imagens/PB_-_Login.png)

# Caso de uso
![](Imagens/UC1_-_Cadastro_de_Usu%C3%A1rio_do_Sistema.png)

# Diagramas de sequência

## Fluxos de acesso autorizado

Com e-mail e senha válido
![](Imagens/Fluxo3%20-%20Acesso%20com%20Senha.png)

Com token válido
![](Imagens/Fluxo4%20-%20Acesso%20com%20Token%20v%C3%A1lido.png)

Com token inválido, porém, com e-mail e senha válidos
![](Imagens/Fluxo4%20-%20Acesso_token_inv%C3%A1lido_email_valido.png)

## Fluxos de acesso negado

Com e-mail inválido
![](Imagens/Fluxo1%20-%20E-mail%20inv%C3%A1lido.png)

Com senha inválida
![](Imagens/Fluxo2%20-%20Senha%20inv%C3%A1lida.png)
